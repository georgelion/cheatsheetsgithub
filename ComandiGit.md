# Comandi per Configurazione Iniziale

### Inizializzazione del Repository
- `git init` 
  - Inizializzatore, crea una cartella `.git` dove git mette tutto.

### Stato del Repository
- `git status`
  - Mostra lo stato dei file; da usare sempre all’inizio e periodicamente per controllare.

### Aggiungere File
- `git add NomeFile.md`
  - Aggiunge file specificati.

### Committare con Commento
- `git commit -m ‘commento’`
  - Esegue un commit con un commento.

### Visualizzare i Messaggi dei Commit
- `git log`
  - Mostra i messaggi dei commit (la “storia” del progetto).

### Attribuire le Modifiche
- `git blame`
  - Determina chi ha apportato l'ultima modifica a ciascuna riga di un file.

# Comandi per Configurazione Nome e Email

### Configurazione Nome Utente
- `git config user.name “nome”`
  - Firma il progetto corrente assegnando il nome.

### Configurazione Email Utente
- `git config user.email “email”`
  - Firma il progetto corrente assegnando l'email.

### Configurazione Globale
- `git config --global`
  - Firma tutti i progetti con le informazioni fornite.

# Vedere Password e Nome Assegnato

### Visualizzare Configurazione
- `vim .git/config`
- `vim git`

# Per Eliminare un File

### Eliminare un File
- `git rm nomeFile`
  - Elimina subito, ma è necessario committare con un commento (`git commit -m “commento”`).

### Committare l'Eliminazione
- `git commit -m “commento”`
  - Obbligatorio per completare l'eliminazione.

### Controllare lo Stato
- `git status`
  - Controlla che tutto sia andato a buon fine.

### Eliminare da Git ma non dal PC
- `git rm --cached nomeFile`
  - Cancella da git ma NON dal pc.

### Ripristinare un File Staged
- `git restore --staged <NOME FILE>`
  - Ripristina il file alla versione staged.

### Rimuovere un File Aggiunto per Errore
- `git rm --cached file`
  - Rimuove il file da git mantenendolo sul PC.

# Per Far Ignorare un File o una Directory a Git

### Creare e Modificare `.gitignore`
- `touch .gitignore`
  - Crea il file nascosto `.gitignore`.
- `vim .gitignore`
  - Aggiungi il nome del file da ignorare.

### Aggiungere e Committare `.gitignore`
- `git add .gitignore`
  - Aggiungi il file delle esclusioni.
- `git commit -m “commento”`
  - Obbligatorio per committare le modifiche.

# Per Spostare/Rinominare un File

### Rinominare un File
- `mv nomeFile nuovoNomeFile`
  - Rinomina `nomeFile` in `nuovoNomeFile`.

### Committare le Modifiche
- `git add .`
  - Commit di tutto ciò che c’è da committare.
- `git commit -m “commento”`
  - Obbligatorio per committare le modifiche.

### Spostare un File
- `git mv nomeFile nuovoNomeFile`
  - Sposta un file, saltando `git add`.

# Comandi di Lettura

### Visualizzare i Commit
- `git log`
  - Mostra quanti commit ci sono e chi li ha fatti.

### Visualizzare lo Storico in Breve
- `git log --oneline`
  - Mostra lo storico dell’ID e il commento in una riga.

### Visualizzare le Differenze
- `git diff`
  - Mostra le modifiche rispetto all'ultimo commit.

### Visualizzare l'Ultimo Commit
- `git show`
  - Mostra l'ultimo commit.

### Visualizzare un Commit Specifico
- `git show <id>`
  - Mostra i cambiamenti da quell'ID ad oggi.

### Differenze tra Due Commit
- `git diff <id1> <id2>`
  - Mostra i cambiamenti tra due commit specifici.

# Creazione e Gestione Branch

### Visualizzare i Branch
- `git branch`
  - Mostra i branch esistenti.

### Unire Due Branch
- `git merge`
  - Unisce due branch.

### Creare un Nuovo Branch
- `git checkout -b NomeBranch`
  - Crea un nuovo branch.

### Tornare al Branch Master
- `git checkout master`
  - Torna al branch master.

### Tornare a un Punto della Storia
- `git checkout <id>`
  - Vedi tutto com’era in quel momento.

### Eliminare un Branch
- `git branch -d NomeBranch`
  - Elimina il branch.
  - `-D` elimina anche le tracce dei server remoti.
  - `-d` elimina solo il branch locale.

# Comandi Aggiuntivi "Scampoli"

### Modificare l'Ultimo Commit
- `git commit --amend`
  - Modifica l'ultimo commit.

### Verificare l'Autore dei Commit
- `git commit sign`
  - Permette di verificare crittograficamente l'autore dei commit.

### Aggiungere in Modo Parziale
- `git add -p`
  - `p` sta per partial: richiede se aggiungere porzioni del file.

### Eliminare i Commit ma Mantenere le Modifiche
- `git reset`
  - Elimina i commit, ma mantiene le modifiche ai file.

### Annullare un Commit
- `git revert`
  - Annulla un commit specifico creando l'opposto.

### Estrarre un Commit Specifico
- `git cherry-pick`
  - Estrae un commit specifico da un ramo e lo applica a un altro.

### Clonare un Repository
- `git clone`
  - Crea una copia locale di un repository git esistente.

### Creare un Tag
- `git tag`
  - Associa una versione (testuale) a un punto del codice (es. `v2.4.8`).

### Usare git stash
- `git stash`
  - Mette da parte temporaneamente le modifiche non committate.
